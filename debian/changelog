ghidra (11.0+ds-0kali1) kali-dev; urgency=medium

  * d/watch: Add repacksuffix
  * New upstream version 11.0+ds
  * Update lintian overrides

 -- Steev Klimaszewski <steev@kali.org>  Sat, 23 Dec 2023 15:38:46 -0600

ghidra (10.4-0kali1) kali-dev; urgency=medium

  * New upstream version 10.4

 -- Steev Klimaszewski <steev@kali.org>  Fri, 29 Sep 2023 20:40:54 -0500

ghidra (10.3.3-0kali1) kali-dev; urgency=medium

  * Exclude .gitattributes
  * New upstream version 10.3.3
  * Update lintian overrides.
  * Prepare for Release

 -- Steev Klimaszewski <steev@kali.org>  Tue, 26 Sep 2023 18:26:49 -0500

ghidra (10.3.2-0kali1) kali-dev; urgency=medium

  [ Steev Klimaszewski ]
  * ci: Disable 32bit arm arch builds.

  [ Sophie Brun ]
  * New upstream version 10.3.2

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 21 Jul 2023 11:14:29 +0200

ghidra (10.2.2-0kali2) kali-experimental; urgency=medium

  * Fix build on arm64 but embedded sevenzipjbinding does not support arm64

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 22 Dec 2022 14:05:43 +0100

ghidra (10.2.2-0kali1) kali-dev; urgency=medium

  * New upstream version 10.2.2
  * Change required version of gradle
  * Switch to openjdk-17-jdk (as required by new upstream release)
  * Update lintian-overrides
  * Bump Standards-Version to 4.6.1 (no changes)

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 20 Dec 2022 14:33:42 +0100

ghidra (10.1.4-0kali2) kali-dev; urgency=medium

  * Replace dependency openjdk-11-jdk-headless with openjdk-11-jdk
    (fix missing libawt_xawt.so)

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 19 Dec 2022 18:01:29 +0100

ghidra (10.1.4-0kali1) kali-dev; urgency=medium

  * New upstream version 10.1.4

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 20 Jun 2022 15:56:13 +0200

ghidra (10.1.2-0kali2) kali-dev; urgency=medium

  * Do not look for *x86_64.zip file (fails on arm64)

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 31 Jan 2022 16:43:51 +0100

ghidra (10.1.2-0kali1) kali-dev; urgency=medium

  * New upstream version 10.1.2
  * Add arm64 arch
  * Update minimal required version of gradle

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 28 Jan 2022 13:54:39 +0100

ghidra (10.1-0kali2) kali-dev; urgency=medium

  * Bump Standards-Version to 4.6.0
  * Update debian/copyright
  * Update lintian-overrides
  * Remove override_dh_gencontrol

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 14 Dec 2021 14:43:17 +0100

ghidra (10.1-0kali1) kali-dev; urgency=medium

  [ Ben Wilson ]
  * Remove template comment and switch spaces to tabs

  [ Sophie Brun ]
  * Update debian/watch
  * New upstream version 10.1 (with fix for CVE-2021-44228)
  * Adapt debian/rules

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 13 Dec 2021 11:45:15 +0100

ghidra (9.1.2-0kali4) kali-dev; urgency=medium

  * Remove arm64: upstream does not support it. Build fails.

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 26 May 2021 08:56:16 +0200

ghidra (9.1.2-0kali3) kali-experimental; urgency=medium

  * Add a patch to support arm64 

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 03 May 2021 17:02:18 +0200

ghidra (9.1.2-0kali2) kali-experimental; urgency=medium

  * Add arm64 to supported architecture

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 03 May 2021 17:02:13 +0200

ghidra (9.1.2-0kali1) kali-dev; urgency=medium

  * Initial release (see 7090)

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 20 Apr 2021 17:47:44 +0200
